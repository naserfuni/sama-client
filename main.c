#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include "client.h"
#include <poll.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <semaphore.h>

#define BILLION  1000000000.0

int call = 0;
int isFirstPRI = 1;
int onHook = 0;
int sockfd, sockfdc;
sem_t mutex;
int has_data;

int msleep(long tms)
{
    struct timespec ts;
    int ret;

    if (tms < 0)
    {
        errno = EINVAL;
        return -1;
    }

    ts.tv_sec = tms / 1000;
    ts.tv_nsec = (tms % 1000) * 1000000;

    do {
        ret = nanosleep(&ts, &ts);
    } while (ret && errno == EINTR);

    return ret;
}

void print_address(struct sockaddr_in *address) {
    printf("Address info is: %d, %d, %d\n", address->sin_addr.s_addr, address->sin_family, address->sin_port);
}

void connect_to_server(int *sockfd, struct sockaddr_in *servaddr) {
    // socket create and varification
    *sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (*sockfd == -1) {
        printf("socket creation failed...\n");
        exit(0);
    } else {
        printf("Socket successfully created..\n");
    }

    // connect the client socket to server socket
    if (connect(*sockfd, (SA*)servaddr, sizeof(*servaddr)) != 0) {
        printf("connection with the server failed...\n");
        exit(0);
    } else {
        printf("connected to the server..\n");
    }
}

void init_server(char *ip_address, int *port, struct sockaddr_in *address) {
    printf("init_server input: %s, %d\n", ip_address, *port);
    address->sin_family = AF_INET;
    address->sin_addr.s_addr = inet_addr(ip_address);
    address->sin_port = htons(*port);
    print_address(address);
}

void read_data(int sockfd, char *buff) {
    int32_t data_size;

    read(sockfd, (char*)&data_size, sizeof(data_size));
    printf("Data size is: %d\n", ntohl(data_size));

    bzero(buff, sizeof(buff));
    read(sockfd, buff, ntohl(data_size));
    buff[ntohl(data_size)] = '\0';
    printf("%s\n", buff);
}

void get_commands(char *buff, int *command, int *value) {
    char command_str[MAX_BUFFER_SIZE];

    if (strcmp(buff, "ioctl:")) {
        memcpy(command_str, &buff[6], MAX_BUFFER_SIZE - 6);
        *command = atoi(strtok(command_str, ","));
        *value = atoi(strtok(NULL, ","));
    }
}

void send_event(int32_t event) {
    char buff[MAX_BUFFER_SIZE];
    int32_t dataSize = 0;

    bzero(buff, MAX_BUFFER_SIZE);
    sprintf(buff, "event:%d", event);
    dataSize = htonl(strlen(buff));
    if (write(sockfdc, (char*)&dataSize, sizeof(dataSize)) >= 0 && write(sockfdc, buff, strlen(buff)) >= 0) {
        printf("Event %s sent.\n", buff);
    } else {
        printf("Error oucerred in send event!\n");
    }
}

char buff_out[MAX_BUFFER_SIZE];

void read_data_thread() {
    char buff[MAX_BUFFER_SIZE];
    int32_t r = 0;
    int32_t counter = 1;
    FILE *f;
    struct timespec start, end, totall;

    while (onHook == 0) {
        clock_gettime(CLOCK_REALTIME, &start);

        printf("Read count %d\n", counter);

        send_event(POLLOUT);

        bzero(buff, MAX_BUFFER_SIZE);
        printf("Wait for read\n");
        r = read(sockfd, buff, 160);
        printf("Read successful with len: %d\n", r);

        f = fopen("in_voice", "a");
        fwrite(buff, r, sizeof(char), f);
        fclose(f);

        sem_wait(&mutex);
        if (has_data) {
            printf("Loose some data with len %d.\n", has_data);
        }
        bzero(buff_out, MAX_BUFFER_SIZE);
        memcpy(buff_out, buff, r);
        has_data = r;
        sem_post(&mutex);

        clock_gettime(CLOCK_REALTIME, &end);
        double time_spent = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / BILLION;
        printf("Read time spent is: %f seconds.\n", time_spent);

        if (isFirstPRI) {
            msleep(20);
        }

        clock_gettime(CLOCK_REALTIME, &totall);
        double totall_time_spent = (totall.tv_sec - start.tv_sec) + (totall.tv_nsec - start.tv_nsec) / BILLION;
        printf("Total Read time is: %f seconds.\n", totall_time_spent);

        counter++;
    }
    printf("Error oucerred in send event!\n");
}

void write_data_thread() {
    char local_buff_out[MAX_BUFFER_SIZE];
    int32_t w = 0;
    FILE *f;
    struct timespec start, end, totall;

    sleep(4);

    while (onHook == 0) {
        clock_gettime(CLOCK_REALTIME, &start);

        sem_wait(&mutex);
        bzero(local_buff_out, MAX_BUFFER_SIZE);
        if (has_data) {
            memcpy(local_buff_out, buff_out, has_data);
            has_data = 0;
        } else {
            printf("Zero data!\n");
        }
        sem_post(&mutex);

        send_event(POLLIN);

        f = fopen("out_voice", "a");
        fwrite(local_buff_out, 160, 1, f);
        fclose(f);

        printf("Wait for write\n");
        w = write(sockfd, local_buff_out, 160);
        printf("Write successful with len: %d\n", w);

        clock_gettime(CLOCK_REALTIME, &end);
        double time_spent = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / BILLION;
        printf("Write time spent is: %f seconds.\n", time_spent);

        msleep(20);

        clock_gettime(CLOCK_REALTIME, &totall);
        double totall_time_spent = (totall.tv_sec - start.tv_sec) + (totall.tv_nsec - start.tv_nsec) / BILLION;
        printf("Total write time is: %f seconds.\n", totall_time_spent);
    }
    printf("Error oucerred in send event!\n");
}

void command_handler(int sockfd, int sockfdc, int command, int value) {
    char buff[MAX_BUFFER_SIZE];
    int32_t event = 0;
    int32_t dataSize = 0;

    bzero(buff, MAX_BUFFER_SIZE);

    switch (command) {
    case SPECIFY:
        printf("My channel number is: %d\n", value);
        break;
    case SET_BLOCKSIZE:
        printf("Set Block size\n");
        call_params.block_size = value;
        printf("Block size %d set.\n", value);
        break;
    case ECHOCANCEL:
        printf("Echo Cancel\n");
        call_params.echo_state = value;
        printf("Echo canceled.\n", value);
        break;
    case GET_BLOCKSIZE:
        printf("Get Block size\n");
        sprintf(buff, "data:%d", call_params.block_size);
        dataSize = htonl(strlen(buff));
        if (write(sockfdc, (char*)&dataSize, sizeof(dataSize)) >= 0 && write(sockfdc, buff, strlen(buff)) >= 0) {
            printf("Block size %s sent.\n", buff);
        } else {
            printf("Error oucerred in send block size!\n");
        }
        break;
    case HOOK:
        switch (value) {
        case SAMA_RING:
            printf("Ring! Ring!\n");
            break;
        case SAMA_RINGOFF:
            printf("Ring off!\n");
            break;
        case SAMA_OFFHOOK:
            printf("OffHook\n");

            FILE *f = fopen("hookoff", "r");
            fseek(f, 0, SEEK_END);
            long fsize = ftell(f);
            fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

            char *string = malloc(fsize + 1);
            fread(string, 1, fsize, f);
            fclose(f);

            string[fsize] = 0;

            int j = 0;
            for (int i = 0; i < fsize && j < 2; i += 1024) {
                send_event(POLLIN);

                write(sockfd, &string[i], 1024);
                j++;
            }

            pthread_t thread_id_read, thread_id_write;
            printf("Before Thread\n");
            pthread_create(&thread_id_read, NULL, read_data_thread, NULL);
            pthread_create(&thread_id_write, NULL, write_data_thread, NULL);
            printf("After Thread\n");
            break;
        case SAMA_ONHOOK:
            printf("OnHook\n");
            onHook = 1;
            break;
        default:
            printf("Unknown hook with value %d!\n", value);
            break;
        }
        break;
    case GETEVENT:
        printf("Get event.\n");

        if (isFirstPRI) {
            sprintf(buff, "data:%d", SAMA_EVENT_RINGBEGIN);
            dataSize = htonl(strlen(buff));
            if (write(sockfdc, (char*)&dataSize, sizeof(dataSize)) >= 0 && write(sockfdc, buff, strlen(buff)) >= 0) {
                printf("SAMA_EVENT_RINGBEGIN sent. Buffer is %s.\n", buff);
            } else {
                printf("Error oucerred in send SAMA_EVENT_RINGBEGIN!\n");
            }
            send_event(POLLPRI);
            printf("send second pri\n");
            isFirstPRI = 0;
        } else {
            sprintf(buff, "data:%d", SAMA_EVENT_RINGOFFHOOK);
            dataSize = htonl(strlen(buff));
            if (write(sockfdc, (char*)&dataSize, sizeof(dataSize)) >= 0 && write(sockfdc, buff, strlen(buff)) >= 0) {
                printf("SAMA_EVENT_RINGOFFHOOK sent. Buffer is %s.\n", buff);
            } else {
                printf("Error oucerred in send SAMA_EVENT_RINGOFFHOOK!\n");
            }

            printf("send dtmf/fsk\n");

            FILE *f = fopen("begin_call.freeswitch", "r");
            fseek(f, 0, SEEK_END);
            long fsize = ftell(f);
            fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

            char *string = malloc(fsize + 1);
            fread(string, 1, fsize, f);
            fclose(f);

            string[fsize] = 0;

//            int j = 55;
            int j = 0;
//            for (int i = 8800; i < fsize && j < 101; i += 160) {
            for (int i = 0; i < fsize && j < 120; i += 160) {
                send_event(POLLIN);

                write(sockfd, &string[i], 160);
                j++;
                msleep(20);
            }
        }

//        pthread_t thread_id_read, thread_id_write;
//        printf("Before Thread\n");
//        pthread_create(&thread_id_read, NULL, read_data_thread, NULL);
//        pthread_create(&thread_id_write, NULL, write_data_thread, NULL);
//        printf("After Thread\n");
        break;
    default:
        printf("Unknown command! command: %d, data: %d\n", command, value);
        break;
    }
}

void answer_call(int sockfd, int sockfdc)
{
    char buff[MAX_BUFFER_SIZE], *command_str;
    int n = 0;

    while (1) {
        int command, value;

        printf("*********************************\n");
        if (call == 0) {
            read_data(sockfdc, buff);
            command_str = strtok(buff, ".end\n");
            while (command_str != NULL) {
                printf("*********************************\n");
                printf("step: %d\n", n);
                get_commands(command_str, &command, &value);
                command_handler(sockfd, sockfdc, command, value);
                command_str = strtok(NULL, ".end\n");
                n++;
            }
        } else {
            int32_t event = 0;
            event = POLLOUT;
            write(sockfdc, (char *)&event, sizeof(event));
            printf("POLLOUT sent.\n");
            bzero(buff, sizeof(buff));
            read(sockfd, buff, ntohl(160));
            printf("Data read!\n");

            event = POLLIN;
            write(sockfdc, (char *)&event, sizeof(event));
            printf("POLLIN sent.\n");
            write(sockfd, buff, strlen(buff));
            printf("Data sent.\n");
            sleep(0.1);
        }
    }
}

void make_call(int sockfd, int sockfdc)
{
    char buff[MAX_BUFFER_SIZE], *command_str;
    int n = 0;

    send_event(POLLPRI);
    printf("send first pri\n");

    while (1) {
        int command, value;

        printf("*********************************\n");
        if (call == 0) {
            read_data(sockfdc, buff);
            command_str = strtok(buff, ".end\n");
            while (command_str != NULL) {
                printf("*********************************\n");
                printf("step: %d\n", n);
                get_commands(command_str, &command, &value);
                command_handler(sockfd, sockfdc, command, value);
                command_str = strtok(NULL, ".end\n");
                n++;
            }
        } else {
            int32_t event = 0;
            event = POLLOUT;
            write(sockfdc, (char *)&event, sizeof(event));
            printf("POLLOUT sent.\n");
            bzero(buff, sizeof(buff));
            read(sockfd, buff, ntohl(160));
            printf("Data read!\n");

            event = POLLIN;
            write(sockfdc, (char *)&event, sizeof(event));
            printf("POLLIN sent.\n");
            write(sockfd, buff, strlen(buff));
            printf("Data sent.\n");
            sleep(0.1);
        }
    }
}

int main(int argc, char **argv)
{
    int port = 8087, cport = 8086;
    char *ip_address = "192.168.1.104";
    struct sockaddr_in server_addr, c_server_addr;
    bzero(&server_addr, sizeof(server_addr));
    bzero(&c_server_addr, sizeof(c_server_addr));
    sem_init(&mutex, 0, 1);
    has_data = 0;

    printf("%s\n", ip_address);

    init_server(ip_address, &port, &server_addr);
    connect_to_server(&sockfd, &server_addr);

    init_server(ip_address, &cport, &c_server_addr);
    connect_to_server(&sockfdc, &c_server_addr);

    if (strcmp(argv[1], "answer") == 0) {
        answer_call(sockfd, sockfdc);
    } else if (strcmp(argv[1], "call") == 0) {
        make_call(sockfd, sockfdc);
    }

    close(sockfd);
    close(sockfdc);
    sem_destroy(&mutex);
}
