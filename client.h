#ifndef CLIENT_H
#define CLIENT_H
#include <sys/ioctl.h>

#define SA struct sockaddr
#define MAX_BUFFER_SIZE 1024000

/* ioctl defines */

#define		SAMA_CODE				'J'

#define		SAMA_GET_BLOCKSIZE	_IOR  (SAMA_CODE, 1, int)					/* Get Transfer Block Size. */
#define		SAMA_SET_BLOCKSIZE	_IOW  (SAMA_CODE, 2, int)					/* Set Transfer Block Size. */
#define		SAMA_FLUSH			_IOW  (SAMA_CODE, 3, int)					/* Flush Buffer(s) and stop I/O */
#define		SAMA_SYNC				_IOW  (SAMA_CODE, 4, int)					/* Wait for Write to Finish */
//#define		SAMA_GET_PARAMS		_IOR  (SAMA_CODE, 5, struct sama_params)	/* Get channel parameters */
//#define		SAMA_SET_PARAMS		_IOW  (SAMA_CODE, 6, struct sama_params)	/* Set channel parameters */
#define		SAMA_HOOK				_IOW  (SAMA_CODE, 7, int)					/* Set Hookswitch Status */
#define		SAMA_GETEVENT			_IOR  (SAMA_CODE, 8, int)					/* Get Signalling Event */
#define		SAMA_IOMUX			_IOWR (SAMA_CODE, 9, int)					/* Wait for something to happen (IO Mux) */
//#define		SAMA_SPANSTAT			_IOWR (SAMA_CODE, 10, struct sama_spaninfo) /* Get Span Status */
//#define		SAMA_MAINT			_IOW  (SAMA_CODE, 11, struct sama_maintinfo)/* Set Maintenance Mode for a span */
//#define		SAMA_GETCONF			_IOWR (SAMA_CODE, 12, struct sama_confinfo)	/* Get Conference Mode */
//#define		SAMA_SETCONF			_IOWR (SAMA_CODE, 13, struct sama_confinfo)	/* Set Conference Mode */
//#define		SAMA_CONFLINK			_IOW  (SAMA_CODE, 14, struct sama_confinfo)	/* Setup or Remove Conference Link */
#define		SAMA_CONFDIAG			_IOR  (SAMA_CODE, 15, int)				/* Display Conference Diagnostic Information on Console */

//#define		SAMA_GETGAINS			_IOWR (SAMA_CODE, 16, struct sama_gains)	/* Get Channel audio gains */
//#define		SAMA_SETGAINS			_IOWR (SAMA_CODE, 17, struct sama_gains)	/* Set Channel audio gains */
//#define		SAMA_SPANCONFIG		_IOW (SAMA_CODE, 18, struct sama_lineconfig)/* Set Line (T1) Configurations and start system  */
//#define		SAMA_CHANCONFIG		_IOW (SAMA_CODE, 19, struct sama_chanconfig)/* Set Channel Configuration  */
//#define		SAMA_SET_BUFINFO		_IOW (SAMA_CODE, 27, struct sama_bufferinfo)/* Set buffer policy */
//#define		SAMA_GET_BUFINFO		_IOR (SAMA_CODE, 28, struct sama_bufferinfo)/* Get current buffer info */
#define		SAMA_AUDIOMODE		_IOW  (SAMA_CODE, 32, int)				/* Set a clear channel into audio mode */
#define		SAMA_ECHOCANCEL		_IOW  (SAMA_CODE, 33, int)				/* Control Echo Canceller */
#define		SAMA_HDLCRAWMODE		_IOW  (SAMA_CODE, 36, int)				/* Set a clear channel into HDLC w/out FCS checking/calculation mode */
#define		SAMA_HDLCFCSMODE		_IOW  (SAMA_CODE, 37, int)				/* Set a clear channel into HDLC w/ FCS mode */

/* Specify a channel on /dev/ftdm/chan -- must be done before any other ioctl's and is only valid on /dev/ftdm/chan */
#define		SAMA_SPECIFY			_IOW  (SAMA_CODE, 38, int)

/* Temporarily set the law on a channel to SAMA_LAW_DEFAULT, SAMA_LAW_ALAW, or SAMA_LAW_MULAW. Is reset on close. */
#define		SAMA_SETLAW			_IOW  (SAMA_CODE, 39, int)

/* Temporarily set the channel to operate in linear mode when non-zero or default law if 0 */
#define		SAMA_SETLINEAR		_IOW  (SAMA_CODE, 40, int)

#define		SAMA_GETCONFMUTE		_IOR  (SAMA_CODE, 49, int)				/* Get Conference to mute mode */
#define		SAMA_ECHOTRAIN		_IOW  (SAMA_CODE, 50, int)				/* Control Echo Trainer */

/* Set/Get CAS bits */
#define SAMA_SETTXBITS _IOW (SAMA_CODE, 43, int)
#define SAMA_GETRXBITS _IOR (SAMA_CODE, 45, int)

/*
 * Enable tone detection -- implemented by low level driver
 */
#define SAMA_TONEDETECT                _IOW(SAMA_CODE, 91, int)

enum ioctlcmd {
    GET_BLOCKSIZE = SAMA_GET_BLOCKSIZE,
    SET_BLOCKSIZE = SAMA_SET_BLOCKSIZE,
    FLUSH = SAMA_FLUSH,
    SYNC = SAMA_SYNC,
//    GET_PARAMS = SAMA_GET_PARAMS,
//    SET_PARAMS = SAMA_SET_PARAMS,
    HOOK = SAMA_HOOK,
    GETEVENT = SAMA_GETEVENT,
    IOMUX = SAMA_IOMUX,
//    SPANSTAT = SAMA_SPANSTAT,
//    MAINT = SAMA_MAINT,
//    GETCONF = SAMA_GETCONF,
//    SETCONF = SAMA_SETCONF,
//    CONFLINK = SAMA_CONFLINK,
    CONFDIAG = SAMA_CONFDIAG,
//    GETGAINS = SAMA_GETGAINS,
//    SETGAINS = SAMA_SETGAINS,
//    SPANCONFIG = SAMA_SPANCONFIG,
//    CHANCONFIG = SAMA_CHANCONFIG,
//    SET_BUFINFO = SAMA_SET_BUFINFO,
//    GET_BUFINFO = SAMA_GET_BUFINFO,
    AUDIOMODE = SAMA_AUDIOMODE,
    ECHOCANCEL = SAMA_ECHOCANCEL,
    HDLCRAWMODE = SAMA_HDLCRAWMODE,
    HDLCFCSMODE = SAMA_HDLCFCSMODE,
    SPECIFY = SAMA_SPECIFY,
    SETLAW = SAMA_SETLAW,
    SETLINEAR = SAMA_SETLINEAR,
    GETCONFMUTE = SAMA_GETCONFMUTE,
    ECHOTRAIN = SAMA_ECHOTRAIN,
    SETTXBITS = SAMA_SETTXBITS,
    GETRXBITS = SAMA_GETRXBITS,
    TONEDETECT = SAMA_TONEDETECT,
};

typedef enum {
        SAMA_EVENT_NONE			= 0,
        SAMA_EVENT_ONHOOK			= 1,
        SAMA_EVENT_RINGOFFHOOK	= 2,
        SAMA_EVENT_WINKFLASH		= 3,
        SAMA_EVENT_ALARM			= 4,
        SAMA_EVENT_NOALARM		= 5,
        SAMA_EVENT_ABORT			= 6,
        SAMA_EVENT_OVERRUN		= 7,
        SAMA_EVENT_BADFCS			= 8,
        SAMA_EVENT_DIALCOMPLETE	= 9,
        SAMA_EVENT_RINGERON		= 10,
        SAMA_EVENT_RINGEROFF		= 11,
        SAMA_EVENT_HOOKCOMPLETE	= 12,
        SAMA_EVENT_BITSCHANGED	= 13,
        SAMA_EVENT_PULSE_START	= 14,
        SAMA_EVENT_TIMER_EXPIRED	= 15,
        SAMA_EVENT_TIMER_PING		= 16,
        SAMA_EVENT_POLARITY		= 17,
        SAMA_EVENT_RINGBEGIN		= 18,
        SAMA_EVENT_DTMFDOWN		= (1 << 17),
        SAMA_EVENT_DTMFUP			= (1 << 18),
} sama_event_t;

struct call_parameters {
    int block_size;
    int echo_state;
} call_params;

typedef enum {
        SAMA_ONHOOK				= 0,
        SAMA_OFFHOOK				= 1,
        SAMA_WINK					= 2,
        SAMA_FLASH				= 3,
        SAMA_START				= 4,
        SAMA_RING					= 5,
        SAMA_RINGOFF				= 6
} sama_hookstate_t;

#endif
